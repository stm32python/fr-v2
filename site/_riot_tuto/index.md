---
layout: article
title: Exercices MicroPython pour la carte P-NUCLEO-WB55
description: Exercices MicroPython avec RIOT OS pour la carte P-NUCLEO-WB55
permalink: tutoriel.html
key: page-aside
cover: site/assets/cover/kit.jpg
aside:
  toc: true
sidebar:
  nav: site_nav

---
# Exercices MicroPython avec RIOT OS pour la carte P-NUCLEO-WB55

Les exercices avec RIOT OS pour la carte P-NUCLEO-WB55 arrivent très vite !

Ce [répertoire](https://gitlab.com/stm32python/assets/-/tree/master/public/RIOT/exercices) contient quelques scripts Micropython pour la carte P-NUCLEO-WB55.

En attendant, vous pouvez suivre les exercices du [cours RIOT](https://github.com/RIOT-OS/riot-course/blob/master/README.md) en utilisant `BOARD=p-nucleo-wb55`.

## Hello World !

Depuis la console (Putty sur Windows, pyterm ou minicom sur Linux/MacOSX), entrez les instructions Python suivantes:
```python
print("Hello World " * 6)
```

Le résultat est :
```python
>>> print("Hello World " * 6)
Hello World Hello World Hello World Hello World Hello World Hello World  
```

## Clignotement des 3 DELs

Bientôt !

## Utilisations des 3 boutons SM1, SM2 et SM3

Bientôt !
