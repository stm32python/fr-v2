---
layout : article
title: Téléchargement
description: Liens de téléchargement des firmwares et exercices
permalink: telechargement.html
cover: site/assets/cover/microPython/download.jpg
key: page-aside
aside:
  toc: true
sidebar:
  nav: site_nav

---

**Outils software**<br>
[Notepad++](https://drive.google.com/drive/folders/1cvxYGfjHUo5_WrzCS3nMCThETnBiXGI0?usp=sharing)<br>
[TeraTerm](https://drive.google.com/drive/folders/1ZHIoUInsrr_ekHqgWNX5gP6gFnjybX6M?usp=sharing)<br>
[Thonny](https://drive.google.com/drive/folders/1iXiL6h0Fg4It9dGXlwMlYXABr6kiJjk5?usp=sharing)<br>
[PuTTY](https://drive.google.com/drive/folders/1QaN4n6_zORZ7P8XOjuydCYZOuSsGiICq?usp=sharing)<br>
[PyScript](https://drive.google.com/drive/folders/1EI5MaYYN4TEkXPOmChPhXUfrTyALq4LH?usp=sharing)<br>

**Firmware**<br>
[Firmware_1.13](https://drive.google.com/drive/folders/1hrllG6rvtkksMqYClUx0DB360AFfaxR5?usp=sharing)

**TP et exemples**<br>
- [GPIO](https://drive.google.com/drive/folders/1DiLyczHdsHKvI42AuZqsXHzCuAQto4HH?usp=sharing) (Utilisez les GPIO au travers d'exemples de lecture/écriture)
    - [BLINK](https://drive.google.com/drive/folders/1OcVAapqjPVz6O-TzgY9D9Nk7M7wX5m2I?usp=sharing) (Faites clignoter une LED avec une pause de 100 ms)<br>
    - [Boutons](https://drive.google.com/drive/folders/1jlvg8n15AOjq-tlPlK1CzQq3AiOeA4ck?usp=sharing) (Récupérez la valeur des 3 boutons de la carte NUCLEO-WB55 au traver du l'UART de l'USB user) ***Exercices basiques*** <br>
    - [Chenillard](https://drive.google.com/drive/folders/1jlvg8n15AOjq-tlPlK1CzQq3AiOeA4ck?usp=sharing) (Faites un chenillard avec les 3 LED de la carte NUCLEO-WB55) ***Exercices basiques*** <br>
- [ADC](https://drive.google.com/drive/folders/1XAsawgi9TZguRd0xsCB6ym-X1msduJkA?usp=sharing) (Lecture d’une valeur analogique)<br>
    - [Potentiomètre](https://drive.google.com/drive/folders/1z81SefKdJfMtB4gO5wsGtSwt0Ft_snV6?usp=sharing) (Lecture de la valeur analogique d'un potentiomètre) ***Exercices basiques*** <br>    
    - [Potentiomètre GROVE](https://drive.google.com/drive/folders/1SBU0z-jxGM6Tuw23r6XrN4WAArMSoHSn?usp=sharing) (Lecture de la valeur analogique du potentiomètre GROVE) ***Exercices potentiomètre GROVE*** <br>    


- [LCD](https://drive.google.com/drive/folders/1pZDNmAUu1uJ1fwTnsKhNiW2FjeUjvQjE?usp=sharing) (Affichage du texte sur un écran LCD)<br>
    - [Afficheur OLED I2C](https://drive.google.com/drive/folders/1duCMNQUrZK77HbquBYPk-xxrFBY1KsW0?usp=sharing) (Affichez du texte sur un écran OLED équipé d'un driver SSD1306) ***Exercices basiques*** <br>
    - [LCD RGB GROVE](https://drive.google.com/drive/folders/1DXiMB_KreojxsrNGuav6uEgeV5DCPgN0?usp=sharing) (Affichez du text sur un écran LCD RGB GROVE)<br><br>
- [Afficheur 7 segments](https://drive.google.com/drive/folders/1ofAKSS4xH9wYaqVOVbevqYDppu7-QzVT?usp=sharing) (Utilisez l'afficheur 7 segments GROVE) ***Exercice afficheur 7 segments GROVE*** <br>
- [Capteurs](https://drive.google.com/drive/folders/1_vTThqJDM9yjbqyhf7piKjsi2Ba9a0Re?usp=sharing) (Utilisez les capteurs de de la carte IKS mais aussi ceux présents dans le kit GROVE)<br>
    - [HTS221 température & humidité (IKS)](https://drive.google.com/drive/folders/1K_Vf-lfnhXh7SyUDHPlAYDzQ3GXq7y8f?usp=sharing) (Affichez la température et l'humidité sur l'UART de l'USB user toutes les secondes)
    - [LIS2DW12 accéléromètre (IKS)](https://drive.google.com/drive/folders/1_QamFKFrW9G1_y81lvvp9izCnwu6OVG-?usp=sharing) (Détectez les accélérations suivant 3 axes orthogonaux) ***Exercices avancés*** <br>
    - [LIS2MDL magnétomètre](https://drive.google.com/drive/folders/1mrBEAMuY4I_0ug1n1Nd2f6v9n64OSSrs?usp=sharing) (Affichez les valeurs du champ magnétique ambiant selon trois axes orthogonaux sur l'UART de l'USB user et programmez une boussole)<br>
    - [LPS22HH température & baromètre](https://drive.google.com/drive/folders/1AA_TNeY3H192iNwOcHG3D6VCz48HpowZ?usp=sharing) (Affichez la température et la pression atmosphérique sur l'UART de l'USB user)<br>
    - [LSM6DSO capteur inertiel (IKS)](https://drive.google.com/drive/folders/1AA_TNeY3H192iNwOcHG3D6VCz48HpowZ?usp=sharing) (Affichez les valeurs du capteur inertiel en mg sur l'UART de l'USB user)<br>
    - [STTS751 température (IKS)](https://drive.google.com/drive/folders/1CnYA3ma8nce4BXFTvUzgwsUgIVHPReZM?usp=sharing) (Affichez la température sur l'UART de l'USB user toutes les secondes)<br>
    - [DS18X20 température (protocole OneWire)](https://drive.google.com/drive/folders/1PcPXOUFS-uhDNxUJoNcKMargf8jrnJpC?usp=sharing) (Affichez la température sur l'UART de l'USB user)<br><br>

- [BLE](https://drive.google.com/drive/folders/1fCdS52mXV_JdiAr9_qPNXRR4T7YEm5BE?usp=sharing) (Utilisez le Bluetooth Low Energy de la carte NUCLEO-WB55 et envoyez des valeurs de aléatoires simulant la température, la pression et l'humidité à l'application mobile STBLESensor) ***Exercices basiques***<br>

- [DEL Infrarouge (GROVE)](https://drive.google.com/drive/folders/1brZTXDqQQnRLv1i4Y-xkD1F-fRYB-cUC?usp=sharing) (Utilisation de la LED IR) ***Exercice DEL infrarouge GROVE***<br>
- [Luminosité (GROVE)](https://drive.google.com/drive/folders/1itcd2G1O9j0yV1GqZISoculRMxS_oA71?usp=sharing) (Utilisation du capteur de luminosité) ***Exercice luminosité GROVE***<br>
- [Buzzer (GROVE)](https://drive.google.com/drive/folders/1Djq0uu6mjPGXdY3nkpXBhYjpPsijHzwy?usp=sharing) (Utilisation du buzzer) ***Exercice buzzer GROVE*** <br>
- [Boite à outils](https://drive.google.com/drive/folders/1Ai3PxDEt_lg25iMSsxLRzbKdU-fQH9zE?usp=sharing) (Retrouvez des morceaux de code qui pourront vous aider : Timers, Interruptions ...)<br>

- [Applications](https://drive.google.com/drive/folders/1q3iUhJ6baRqnyXtEBbkm1Hl4xi8gG31Z?usp=sharing) (Des exemples un peu plus complexes)<br>
    - [BLE casting](https://drive.google.com/drive/folders/1FPvQwvTw085IOhLFaMeLIHgwr5JS6unF?usp=sharing) (Consultez les valeurs des capteurs environnementaux de la carte IKS01A3 sur l'application mobile STBLESensor ainsi que sur un afficheur LCD)<br>
    - [Alarme de mouvement](https://drive.google.com/drive/folders/17-AiX-s7BcV0JSwIUTA1IoGFgAVjD05N?usp=sharing) (Réaliser une alarme de mouvement avec un capteur PIR et un buzzer) ***Exercice alarme GROVE*** <br>
