---
layout: article
title: Reconstruction du firmware MicroPython
description: Comment reconstruire MicroPython avec RIOT OS
permalink: build.html
key: page-aside
cover: site/assets/cover/install.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

**Outils nécessaires à l'installation de MicroPython :**  
Il est nécessaire d'utiliser un __ordinateur Windows avec une machine virtuelle Linux__ installée ou un _ordinateur Linux directement_.

## Installation de la chaine de développement pour RIOT OS pour STM32 sur un ordinateur Linux  

Depuis le bureau Linux, ouvrez un terminal en faisant
Clic Droit puis « Ouvrir un terminal ici »  
Entrez ensuite les commandes suivantes une par une
afin d’installer les logiciels pré-requis.  

```bash
sudo apt-get install git
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
sudo apt-get install cmake
sudo apt-get install lisusb-1.0
```
C'est necessaire aussi d'intaller l'outil __ST-LINK-UTILITY__
```bash
git clone https://github.com/texane/stlink
cd stlink
make release
cd build/Release
sudo make install
cd
sudo ldconfig
```

Pour vérifier si l'outil est bien installé, vous pouvez utiliser le commande __*`st-info`*__

Une fois les logiciels pré-requis installées, il est nécessaire de récupérer le projet RIOT depuis l’outil git en écrivant dans un terminal (ouvert depuis le bureau comme précédemment) les commandes suivantes:

```bash
git clone https://github.com/RIOT-OS/RIOT
```

## Construction en chargement de l'interpréteur Micropython sur la carte Nucleo F446RE

Construisez le firmware contenant l'interpréteur Micropython pour votre carte (ici `nucleo-f446re`).
```bash
cd RIOT
cd examples/micropython
make BOARD=nucleo-f446re
```

Chargez le firmware sur votre carte (ici `nucleo-f446re`).
```bash
make BOARD=nucleo-f446re flash-only
```
> Remarque: `make` execute OpenOCD pour le chargement.

Vous pouvez vous connecter au port console de la carte.

## Construction en chargement de l'interpréteur Micropython sur la carte P-Nucleo-WB55

Le portage de RIOT sur la carte P-Nucleo-WB55 est partiel : la liste des fonctionnalités disponibles est dans le document [doc.txt](https://github.com/RIOT-OS/RIOT/blob/master/boards/p-nucleo-wb55/doc.txt) de la carte.

Construisez le firmware contenant l'interpréteur Micropython pour votre carte (ici `p-nucleo-wb55`).

```bash
cd RIOT
cd examples/micropython
make BOARD=p-nucleo-wb55
```

Chargez le firmware sur votre carte (ici `p-nucleo-wb55`).
```bash
make BOARD=p-nucleo-wb55 flash-only
```
> Remarque: `make` execute OpenOCD pour le chargement.

Vous pouvez vous connecter au port console de la carte.

Il se peut que le chargement du firmware avec OpenOCD échoue : Chargez alors le firmware en utilisant l'application [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) préalablement installée.

Le binaire à charger est situé dans `examples/micropython/bin/p-nucleo-wb55/micropython.bin`.

Une autre alternative pour charger le firmware est de copier le binaire sur le volume `NOD_WB55RG` de la carte soit via l'explorateur de fichiers, soit via la commande `copy` ou `cp` avec la ligne de commande suivante:
```bash
cp bin/p-nucleo-wb55/micropython.bin /Volumes/NOD_WB55RG/
```

## Personnalisation des instructions exécutées au démarrage

Le fichier `boot.py` contient des instructions Python qui sont exécutées au démarrage de la carte avant le passage en mode interactif (REPL).

Il peut être compléter à votre guise.

Il faut alors reconstruire et charger le nouveau firmware sur la carte.
