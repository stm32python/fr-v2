---
layout: article
title: Capteur de pression, température et humidité BME280
description: Mise en oeuvre un module Grove BME280 sur bus I2C en Micropython
permalink: bme280.html
key: page-aside
cover: site/assets/cover/duino/bme280.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

Ce tutoriel explique comment mettre en oeuvre un module Grove BME280 sur bus I2C en Micropython. Le BME280 est un capteur de  de pression, température et humidité.

**Prérequis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un  module Grove BME280

**Le module BME280 :**

Branchez le module sur l'un des connecteurs I2C du Grove base shield.

<div align="center">
<img alt="Grove bme280" src="site/assets/cover/duino/bme280.jpg" width="300px">
</div>

**Le code MicroPython :**

Ce code est adapté de [l'exemple fourni ici](https://github.com/catdog2/mpy_bme280_esp8266).

Les fichiers nécessaires au bon fonctionnement du module sont disponibles dans la [zone de téléchargement](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement). Il faut ajouter le fichier *bme280.py* dans le répertoire du périphérique PYBLASH.<br>

Editez maintenant le script *main.py* :

```python
# Exemple adapté de https://github.com/catdog2/mpy_bme280_esp8266
# Objet du script : Mise en oeuvre du module grove I2C capteur de pression,
# température et humidité basé sur le capteur BME280

import time
from machine import I2C, Pin
import bme280

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
bme = bme280.BME280(i2c=i2c)

while True:

	# Temporisation d'une seconde
	time.sleep_ms(1000)
	
	# Lecture des valeurs mesurées
	bme280data = bme.values
	
	# Séparation et formattage (arrondis) des mesures
	temp = round(bme280data[0],1)
	press = int(bme280data[1])
	humi = int(bme280data[2])

	# Affichage des mesures
	print('=' * 40)  # Imprime une ligne de séparation
	print("Température : " + str(temp) + " °C")
	print("Pression : " + str(press) + " hPa")
	print("Humidité relative : " + str(humi) + " %")
```
**Sortie sur le port série de l'USB USER :**

<div align="center">
<img alt="Grove -BME280 sortie" src="site/assets/images/stm32duino/grove_bme280_output.png" width="450px">
</div>

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
