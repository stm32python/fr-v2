---
layout: article
title: Thermistance
description: Mise en oeuvre de la lecture de la température avec une thermistance
permalink: thermistance.html
key: page-aside
cover: /site/assets/cover/home_page/home.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

Ce tutoriel explique comment récupérer et afficher une température en utilisant une thermistance avec MicroPython.

<h2>Description</h2>

Une thermistance est simple à utiliser, peu coûteuse, avec une précision correcte qui la rend pratique pour certains projets. Les stations météo, les systèmes domotiques et les circuits de contrôle et de protection des équipements sont des applications où les thermistances trouvent une place idéale. On en retrouve également dans des projets dédiés au domaine spatial avec des sondes utilisant des thermistances avec une forte précision.

Une thermistance (ou thermistor en anglais) est une résistance variable qui modifie sa valeur de résistance en fontion de la température. La tension à ses bornes va varier, il faut donc utiliser une patte analogique. Il en existe plusieurs types, classés en fonction de la manière dont leur résistance réagit aux changements de température. On y retrouve
- Des thermistances à coefficient de température négatif (NTC) : la résistance diminue avec une augmentation de la température.
- Des thermistances à coefficient de température positif (PTC) : la résistance augmente avec une augmentation de la température.
Les plus communes sont les NTC et c'est également celles que nous utiliserons dans ce tutoriel. Les NTC sont fabriquées à partir d'un matériau semi-conducteur (comme l'oxyde métallique ou la céramique) qui est chauffé et compressé pour former un matériau conducteur sensible à la température.

Dans cette exemple nous allons voir comment récupérer la température puis l'afficher dans un terminal série en degré Kelvin mais également degré Celsius.


<h2>Montage</h2>

Pour le montage nous utilisons une thermistance ainsi qu'une résistance et c'est tout ! Cependant avant de câbler nous avons besoin de connaitre la valeur de résistance de la thermistance. Pour cela vous pouvez utiliser la documentation du composant ou sinon utiliser un multimètre (en position ohmmètre). Généralement il n'existe que deux valeurs : 10kOhms et 100kOhms. Relevez la valeur et arrondissez la au plus proche par rapports aux deux valeurs précédentes (par exemple on relève 13kOhms au multimètre, alors c'est une 10kOhms). Une fois cette valeur connue on vient choisir une résistance avec la même valeur que celle de la thermistance.

**Remarque :** Dans ce tutoriel nous utiliserons une thermistance de 10kOhms.

On vient réaliser le montage suivant :

<div align="center">
<img alt="Schéma de montage thermistance" src="site/assets/images/stm32duino/thermistance-schema.png" width="800px">
</div>

On alimente le montage avec une tension de 3,3V et on utilise le port A1 de la carte NUCLEO-WB55.


<h2>Programme</h2>

La thermistance est un composant très simple à utiliser, son code est donc tout aussi simple. Il faut tout de même avoir quelques notions sur le convertisseur analogique numérique (ADC ou CAN), la loi d'Ohm et le pont diviseur de tension. Cependant ne vous faites pas de soucis, nous reviendrons sur ces différents éléments lors de la programmation.

**Etape 1 :** On importe les bibliothèques dans notre code. On retrouve ainsi une bibliothèque pour le convertisseur analogique numérique mais aussi une bibliothèque pour le temps pour afficher l'intervalle de temps entre deux mesures. Enfin on importe une bibliothèque math pour utiliser le logarithme pendant les calcul
```python
import pyb
from pyb import ADC, Pin
import time
import math
```

**Etape 2 :** Dans un premier temps on vient remplir les caractéristiques de notre circuit. On retrouve une tension d'alimention (**Vref**) de 3,3V mais également la plage de valeurs de l'ADC (**echADC**) qui se calcul grâce à la formule echADC = 2^nbitadc - 1 avec nbitadc = 12 bits. Ensuite on caractérise la thermistance à l'aide de 3 valeurs issues de la documentation technique. Respectivement on retrouve **R** la résistance caracteristique de la thermistance à 25°C (ici 10kOhms), **B** son indice thermique et **T1** sa température absolue.
```python
#Tension d'alimentation du circuit de 3.3V
Vref = 3.3

#Plage de valeurs (echelle) de l'ADC : 2^12 - 1 = 4095
echADC = 4095.0

#Caracteristique de la thermistance utilisée
R = 10			#Résistance à 25°C : 10kOhms
B = 3950.0		#Indice thermique
T1 = 25			#Temperature absolue en °K
```

Dans un second temps on vient définir la patte A1 de la carte en tant qu'entrée analogique pour lire la valeur de tension de la thermistance. On définit également une variable (**oldtempC**) qui nous permettra de stocker l'ancienne valeur de temperature pour la comparer à la nouvelle valeur. Enfin on crée un variable (**temp_s**) qui stockera le temps en secondes au lancement du programme.
```python
#Pin A1 en entrée analogique
adc_A1 = ADC(Pin('A1'))

oldtempC = 0.0

temps_s = time.time()
```

**Etape 3 :** Dans un premier nous allons acquérir la température. Pour cela on vient récupérer la valeur de tension grâce à l'ADC et on stocke cette valeur dans la variable **valeur**. Pour ensuite convertir cette valeur en tension on applique la formule suivante : tension = valeur / echADC x Vref = valeur / 4095 x 3,3.
Ensuite on utilise la formule du pont diviseur de tension pour obtenir la valeur de résistance de la thermistance : Vout = Vin x (R2 / (R1 + R2)) que l'on simplifie de cette facon R2 = R1 x (Vin / Vout) et donc dans notre cas : Rt = R x (tension / (Vref - tension)).
Enfin à partir de cette formule on calcule la température en degré Kelvin avec la formule : tempK = 1/(1/(273.15 + T1) + math.log(Rt/R)/B).

**Remarque** : la dernière formule est générale. Cependant certains constructeurs fournissent leur propre formule avec leurs propres coefficients. Celle-ci peut donc être différente mais le principe reste le même.
```python
while True:
	#Lecture de la pin A1
	valeur = adc_A1.read()
	#Calcul de la tension
	tension = valeur / echADC * Vref
	#Calcul de la valeur de resistance de la thermistance
	Rt = R * tension / (Vref - tension)
	#Calcul des temperature en degré Kelvin et degré Celsius
	tempK = 1/(1/(273.15 + T1) + math.log(Rt/R)/B)
	tempC = tempK - 273.15
```

Finalement et toujours dans la boucle *while* on vient faire l'affichage. On souhaite afficher la température à chaque fois que celle-ci change à ±0,2°C afin d'avoir un affichage clair et propre. Si cette valeur est différente alors on la stocke dans la variable **oldtempC** puis on affiche le temps en secondes et les températures en degré Celsius et Kelvin.
```python
	#Affichage du resultat (si valeur dépassée à 0,2 près)
	if((tempC < (oldtempC-0.2)) or (tempC > (oldtempC+0.2))):
		oldtempC = tempC
		print("-"*21, "Temps :", time.time()-temps_s , "s", "-"*21)
		print("Temperature : %.2f°C\t\tTemperature : %.2f°K" %(tempC, tempK))
	#Temps de pause
	time.sleep(1)
```


<h2>Résultat</h2>

Il ne vous reste plus qu'à sauvegarder l'ensemble puis à redémarrer votre carte NUCLEO-WB55.
Placez votre doigt sur la thermistance pour la chauffer ou soufflez dessus pour la refroidir. Vous devriez alors obtenir un résultat similaire à celui-ci :

<div align="center">
<img alt="Affichage de la température" src="site/assets/images/stm32duino/thermistance-python.PNG" width="50%">
</div>

**Remarque :** On obtient bien la température mais on remarque qu'elle n'est pas des plus précise. En effet la précision de celle-ci dépend de la précision de la thermistance, de la résistance utilisée dans le montage mais également de la précision du calcul dans le programme.
