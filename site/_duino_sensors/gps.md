---
layout: article
title: Module GPS SIM28 Grove en C/C++
description: Exercice avec le module GPS SIM28 Grove en C/C++ avec Stm32duino
permalink: gps.html
key: page-aside
cover: site/assets/cover/soon.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---
<div align="center">
<img alt="Grove - GPS" src="images/grove-gps.jpg" width="400px">
</div>

Bientôt disponible

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
